/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.net;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import android.os.Parcelable;
import android.os.Parcel;
import java.util.ArrayList;
import java.net.InetAddress;
import java.util.List;

/**
 * A simple object for retrieving the results of a DHCP request.
 */
public class DhcpInfo implements Parcelable {
    public int ipAddress;
    public int gateway;
    public int netmask;

    public int dns1;
    public int dns2;

    public int serverAddress;
    public int leaseDuration;

    public List<Dhcp6Lease> dhcp6Leases;

    public DhcpInfo() {
        super();
	dhcp6Leases = new ArrayList<Dhcp6Lease>();
    }

    /** copy constructor {@hide} */
    public DhcpInfo(DhcpInfo source) {
        if (source != null) {
            ipAddress = source.ipAddress;
            gateway = source.gateway;
            netmask = source.netmask;
            dns1 = source.dns1;
            dns2 = source.dns2;
            serverAddress = source.serverAddress;
            leaseDuration = source.leaseDuration;
	    dhcp6Leases = new ArrayList<Dhcp6Lease>();
	    for(Dhcp6Lease lease : source.dhcp6Leases) {
		dhcp6Leases.add(new Dhcp6Lease(lease));
	    }
        }
    }

    public String toString() {
        StringBuffer str = new StringBuffer();

        str.append("ipaddr "); putAddress(str, ipAddress);
        str.append(" gateway "); putAddress(str, gateway);
        str.append(" netmask "); putAddress(str, netmask);
        str.append(" dns1 "); putAddress(str, dns1);
        str.append(" dns2 "); putAddress(str, dns2);
        str.append(" DHCP server "); putAddress(str, serverAddress);
        str.append(" lease ").append(leaseDuration).append(" seconds");

	for(Dhcp6Lease lease : dhcp6Leases) {
	    str.append(lease.toString());
	}

        return str.toString();
    }

    private static void putAddress(StringBuffer buf, int addr) {
        buf.append(NetworkUtils.intToInetAddress(addr).getHostAddress());
    }

    public void updateDhcpv6Leases() {
	dhcp6Leases = new ArrayList<Dhcp6Lease>();
	File directory = new File("/data/dhcp");
	Pattern pattern = Pattern.compile("dhcpv6_.*\\.leases");
	for(File file: directory.listFiles()) {
	    Matcher matcher = pattern.matcher(file.getName());
	    if(matcher.matches()) {
		try {
		    BufferedReader reader = new BufferedReader(new FileReader(file));
		    String line;
		    while((line = reader.readLine()) != null) {
			String[] fields = line.split(" ");
			Dhcp6Lease lease = new Dhcp6Lease();
			lease.ipAddress = NetworkUtils.numericToInetAddress(fields[0]);
			lease.prefixLength = Integer.parseInt(fields[1]);
			lease.leaseDuration = Integer.parseInt(fields[2]);
			lease.prefixColor = Integer.parseInt(fields[3]);
			dhcp6Leases.add(lease);
		    }
		}
		catch (IOException e) {
		}
	    }
	}
    }

    /** Implement the Parcelable interface {@hide} */
    public int describeContents() {
        return 0;
    }

    /** Implement the Parcelable interface {@hide} */
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ipAddress);
        dest.writeInt(gateway);
        dest.writeInt(netmask);
        dest.writeInt(dns1);
        dest.writeInt(dns2);
        dest.writeInt(serverAddress);
        dest.writeInt(leaseDuration);
    }

    /** Implement the Parcelable interface {@hide} */
    public static final Creator<DhcpInfo> CREATOR =
        new Creator<DhcpInfo>() {
            public DhcpInfo createFromParcel(Parcel in) {
                DhcpInfo info = new DhcpInfo();
                info.ipAddress = in.readInt();
                info.gateway = in.readInt();
                info.netmask = in.readInt();
                info.dns1 = in.readInt();
                info.dns2 = in.readInt();
                info.serverAddress = in.readInt();
                info.leaseDuration = in.readInt();
                return info;
            }

            public DhcpInfo[] newArray(int size) {
                return new DhcpInfo[size];
            }
        };
}
