/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.net;

import android.os.Parcelable;
import android.os.Parcel;
import java.net.InetAddress;

public class Dhcp6Lease implements Parcelable {
    public InetAddress ipAddress;
    public int prefixLength;
    public int prefixColor;

    public InetAddress dns1;
    public InetAddress dns2;

    public int leaseDuration;

    public Dhcp6Lease() {
	super();
    }

    public Dhcp6Lease(Dhcp6Lease source) {
	if (source != null) {
	    ipAddress = source.ipAddress;
	    prefixLength = source.prefixLength;
	    dns1 = source.dns1;
	    dns2 = source.dns2;
	    leaseDuration = source.leaseDuration;
	}
    }

    public String toString() {
	StringBuffer str = new StringBuffer();

	str.append("IPv6 address: ").append(ipAddress.getHostAddress());
	str.append(" prefixLength ").append(prefixLength);
	str.append(" dns1 ").append(dns1.getHostAddress());
	str.append(" dns2 ").append(dns2.getHostAddress());
	str.append(" lease ").append(leaseDuration).append(" seconds");

	return str.toString();
    }

    public int describeContents() {
	return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
	dest.writeString(ipAddress.getHostAddress());
	dest.writeInt(prefixLength);
	dest.writeString(dns1.getHostAddress());
	dest.writeString(dns2.getHostAddress());
	dest.writeInt(leaseDuration);
    }

    public static final Creator<Dhcp6Lease> CREATOR = 
	new Creator<Dhcp6Lease>() {
            public Dhcp6Lease createFromParcel(Parcel in) {
		Dhcp6Lease lease = new Dhcp6Lease();
		lease.ipAddress = NetworkUtils.numericToInetAddress(in.readString());
		lease.prefixLength = in.readInt();
		lease.dns1 = NetworkUtils.numericToInetAddress(in.readString());
		lease.dns2 = NetworkUtils.numericToInetAddress(in.readString());
		lease.leaseDuration = in.readInt();
		return lease;
	    }
	    
	    public Dhcp6Lease[] newArray(int size) {
		return new Dhcp6Lease[size];
	    }
    };
}